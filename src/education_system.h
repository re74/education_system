#ifndef EDUCATION_SYSTEM_H
#define EDUCATION_SYSTEM_H
#include <vector>
#include <string>
#include "student.h"
#include "teacher.h"
#include "course.h"
#include "program.h"
#include "department.h"
#include "connection.h"

class Education_System
{
    private:
        int last_student_id = 0;
        int last_teacher_id = 0;
        int last_course_id = 0;
        int last_program_id = 0;
        int last_department_id =0;        

        std::vector<Connection *> teacher_course;
        std::vector<Connection *> student_course;
        std::vector<Connection *> teacher_department;
        
        template <class A>
        void Save_entities_to_file(std::string file_name, std::vector<Entity*> &v);
        template <class A>
        void Read_entities_from_file(std::string file_name, std::vector<Entity*> &v); 
        
        void Save_connections_to_file(std::string file_name, std::vector<Connection *> & v);         
        void Read_connections_from_file(std::string file_name, std::vector<Connection *> &v, std::vector<Entity *> &v1 , std::vector<Entity *> &v2); 
        
        void Save_latest_id_to_file (std::string file_name);        
        void Read_latest_id_from_file(std::string file_name);

    public:
        std::vector<Entity *> teachers;
        std::vector<Entity *> students;
        std::vector<Entity *> courses;
        std::vector<Entity *> departments;
        std::vector<Entity *> programs;
        Education_System();
        ~Education_System();
        void Save ();

        bool Add_teacher(std::string f_name, std::string s_name, float salary);
        bool Add_student(std::string f_name, std::string s_name);
        bool Add_course(std::string name, int points);
        bool Add_program(std::string name, float acceptance_rate);
        bool Add_department(std::string name);

        bool Remove_teacher(int id); 
        bool Remove_student(int id);
        bool Remove_course(int id); 
        bool Remove_program(int id);
        bool Remove_department(int id); 
        
        Entity * Get_entity(int id, std::vector<Entity*> &v);
       
        bool Assign_teacher_to_department (int teacher_id, int department_id); 
        bool Assign_teacher_to_course (int teacher_id, int course_id); 
        bool Assign_student_to_course (int student_id, int course_id);
        
        bool Remove_teacher_from_department (int teacher_id, int department_id);
        bool Remove_teacher_from_course (int teacher_id, int course_id);
        bool Remove_student_from_course (int student_id, int course_id);        

        void Print_teacher_courses (int teacher_id);
        void Print_student_courses (int student_id);       
        void Print_course_students (int course_id);
        void Print_course_teachers (int course_id);
        void Print_teacher_departments(int teacher_id);
        void Print_department_teachers (int department_id);
        void Print_system();
};
#endif