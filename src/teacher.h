#ifndef TEACHER_H
#define TEACHER_H
#include "person.h"
#include "entity.h"

class Teacher : public Person, public Entity
{
    private:
        int id;      
        float salary;        
    public:
        void Print();
        Teacher(){};
        Teacher(std::string f_name, std::string s_name, float salary, int id);
        int Get_id() {return id; }
        int Get_salary() { return id; }
        template <class B>
        void serialize(B& buf) const 
        {
            buf << id << f_name << s_name << salary;
        }
        template <class B>
        void parse(B& buf) 
        {
            buf >> id >> f_name >> s_name >> salary;
        }
};
#endif