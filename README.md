# Education system

This program is an assignment for C++ course.
The program mocks an education system that consists of departments, teachers, students, courses and programs.
Between the runs the system is saved to files into *db_hps* folder.




---
**DO NOT** explicitly change the contents of the *db_hps* folder!

---
## Features
The program functionality includes:
- Operations on teachers 
    - add teacher
    - remove teacher
    - assign teacher to a course
    - remove teacher from the course
    - assign teacher to a department
    - display teacher
- Operations on students 
    - add student
    - remove student
    - assign student to a course
    - remove student from the course
    - display student
- Operations on courses
    - add course
    - remove course
    - display course
- Operations on programs 
    - add program
    - remove program
    - display program
- Operations on departments
    - add department
    - remove department
    - display department
Since the program is a console application, mind that the request output can be placed a bit higher than cursor.


## Usage

1. Download or clone the repository.
2. In the repository folder clone [A C++11 High Performance Serialization Library](https://github.com/jl2922/hps)
2. Install **CMake**:
```sh
$ sudo apt install cmake
```
3. Create a build folder in the repository:
```sh
mkdir build
cd build
```
4. Run the following commands:
```sh
cmake ../
make
./main
```
5. Follow the instructions on the screen. Mind that the ouput of the request can be a bit higher than current coursor position.
When requested to enter id of any entity, use the numbers that are presented on the left side of the output (see the [example](#Output-example)).


### Output example
```sh
==================================

Departments:
0: 08082 Programming
1: 17785 Music
2: 26568 Administration

Teachers:
4: Willy Wonka, salary: 100
5: Santa Claus, salary: 143
6: Julius Caesar, salary: 1000.67
7: Ennio Morricone, salary: 230.6
8: Hans Zimmer, salary: 123
9: Peter Laurence, salary: 23

Students:
1: Maria Nema
2: Paul Pacman
3: Christofer Li Tornton
4: Elsa Wilkinson

Courses:
0: C++, 23 credits
1: C#, 12 credits
2: Python, 16 credits
3: Awesome cypher making, 1 credits

Programs:
0: Bachelor, acceptance: 0.9
2: PhD, acceptance: 0.4
3: Master, acceptance: 0.7

What do you want to do? Press the corresponding key.
T - Teachers: add / remove / see / assign to or remove from course or department
S - Students: add / remove / see / assign to or remove from course
C - Courses: add / remove / see
D - Departments: add / remove / see
P - Programs: add / remove / see
Q - exit
t
==================================
Teachers:
4: Willy Wonka, salary: 100
5: Santa Claus, salary: 143
6: Julius Caesar, salary: 1000.67
7: Ennio Morricone, salary: 230.6
8: Hans Zimmer, salary: 123
9: Peter Laurence, salary: 23

What do you want to do? Press the corresponding key
A - add teacher
R - remove teacher by id
I - see info about teacher by id
C - assign teacher to a course by id
D - assign teacher to a department by id
X - remove teacher from a course by id
Z - remove teacher from a department by id
B - back
r
Enter teacher id: 4
Removed

==================================
Teachers:
5: Santa Claus, salary: 143
6: Julius Caesar, salary: 1000.67
7: Ennio Morricone, salary: 230.6
8: Hans Zimmer, salary: 123
9: Peter Laurence, salary: 23

What do you want to do? Press the corresponding key
A - add teacher
R - remove teacher by id
I - see info about teacher by id
C - assign teacher to a course by id
D - assign teacher to a department by id
X - remove teacher from a course by id
Z - remove teacher from a department by id
B - back
b
==================================

Departments:
0: 08082 Programming
1: 17785 Music
2: 26568 Administration

Teachers:
5: Santa Claus, salary: 143
6: Julius Caesar, salary: 1000.67
7: Ennio Morricone, salary: 230.6
8: Hans Zimmer, salary: 123
9: Peter Laurence, salary: 23

Students:
1: Maria Nema
2: Paul Pacman
3: Christofer Li Tornton
4: Elsa Wilkinson

Courses:
0: C++, 23 credits
1: C#, 12 credits
2: Python, 16 credits
3: Awesome cypher making, 1 credits

Programs:
0: Bachelor, acceptance: 0.9
2: PhD, acceptance: 0.4
3: Master, acceptance: 0.7

What do you want to do? Press the corresponding key.
T - Teachers: add / remove / see / assign to or remove from course or department
S - Students: add / remove / see / assign to or remove from course
C - Courses: add / remove / see
D - Departments: add / remove / see
P - Programs: add / remove / see
Q - exit
q

```

## Dependencies
- [A C++11 High Performance Serialization Library](https://github.com/jl2922/hps)

## Maintainers

@MariaNema
